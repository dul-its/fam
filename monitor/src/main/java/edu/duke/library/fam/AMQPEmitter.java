package edu.duke.library.fam;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

import java.io.IOException;
import java.net.URI;
import java.util.concurrent.TimeoutException;

public class AMQPEmitter implements Emitter {

    private final String amqpQueue;
    private final Connection connection;

    public AMQPEmitter() throws Exception {
        this.amqpQueue = System.getenv("FAM_QUEUE");
	if (amqpQueue == null) {
            throw new RuntimeException("FAM_QUEUE is not set.");
	}

	String amqpUrl = System.getenv("AMQP_URL");
	if (amqpUrl == null) {
            throw new RuntimeException("AMQP_URL is not set.");
	}

        ConnectionFactory factory = new ConnectionFactory();
        factory.setUri(URI.create(amqpUrl));
        factory.setAutomaticRecoveryEnabled(true);

        Connection conn;
        int connTries = 0;
         while (true) {
            try {
                conn = factory.newConnection();
                System.out.println("Connected to RabbitMQ: " + conn.toString());
                break;
            } catch (java.net.ConnectException e) {
                System.out.println("Connection error: " + e.getMessage());
                if (++connTries > 3) throw e;
                else Thread.sleep(10000);
            }
         }
        this.connection = conn;
    }

    @Override
    public void emit(Alteration alteration) throws IOException, TimeoutException {
        Channel channel = getConnection().createChannel();
        System.out.println("RabbitMQ channel opened.");
        channel.queueDeclare(getAmqpQueue(), true, false, false, null);
        String message = alteration.toString();
        System.out.println(message);
        channel.basicPublish("", getAmqpQueue(), null, message.getBytes());
        System.out.println("File alteration message published.");
        channel.close();
        System.out.println("RabbitMQ channel closed.");
    }

    @Override
    public String toString() {
	return "AMQPEmitter publishing to "
	    + getAmqpQueue()
	    + " queue at "
	    + getConnection().getAddress().toString();
    }

    private Connection getConnection() {
	return connection;
    }

    public String getAmqpQueue() {
        return amqpQueue;
    }
}
