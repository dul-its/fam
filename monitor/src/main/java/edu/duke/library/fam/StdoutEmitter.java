package edu.duke.library.fam;

public class StdoutEmitter implements Emitter {

    @Override
    public void emit(Alteration alteration) {
        System.out.println(alteration.toString());
    }

    @Override
    public String toString() {
	return "StdoutEmitter";
    }

}
