/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.duke.library.fam;

import org.apache.commons.cli.*;
import org.apache.commons.io.monitor.FileAlterationMonitor;
import org.apache.commons.io.monitor.FileAlterationObserver;

import java.io.File;
import java.net.URI;

public class Command {

    public static final Integer DEFAULT_INTERVAL = 1000; // in millisecs

    public static void main(String[] args) throws Exception {

	Emitter emitter = null;
	String emitterType = System.getenv("FAM_EMITTER");
	if (emitterType != null) {
	    if (emitterType.equalsIgnoreCase("solr")) {
		emitter = new SolrEmitter();
	    } else if (emitterType.equalsIgnoreCase("amqp")) {
		emitter = new AMQPEmitter();
	    }
	}
	if (emitter == null) {
	    emitter = new StdoutEmitter();
	}
	System.out.println(emitter.toString());

        String path = System.getenv("FAM_PATH");
        if (path == null || path.isEmpty()) {
            throw new RuntimeException("A non-empty path is required.");
        }
        File file = new File(path);
        if (!file.exists()) {
            throw new RuntimeException("Path does not exist.");
        }
        if (!file.isDirectory()) {
            throw new RuntimeException("Path is a file; please specify a directory path.");
        }

        Integer interval = DEFAULT_INTERVAL;
        String intervalStr = System.getenv("FAM_INTERVAL");
        if (intervalStr != null) {
            interval = Integer.parseInt(intervalStr);
        }

        Listener listener = new Listener(emitter);
        FileAlterationObserver observer = new FileAlterationObserver(file);
        observer.addListener(listener);
        FileAlterationMonitor monitor = new FileAlterationMonitor(interval, observer);

        System.out.println("Starting file alteration monitor ...");
        monitor.start();
        System.out.println("File alteration monitor started.");
        try {
            Thread.sleep(Long.MAX_VALUE);
        } catch (InterruptedException e) {
            System.out.println("Stopping file alteration monitor ...");
            monitor.stop(0);
            System.out.println("File alteration monitor stopped.");
        }

    }

}
