package edu.duke.library.fam;

import java.util.UUID;
import java.io.IOException;
import javax.json.Json;
import javax.json.JsonObject;
import java.time.format.DateTimeFormatter;
import org.apache.solr.client.solrj.SolrClient;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.impl.HttpSolrClient;
import org.apache.solr.client.solrj.response.SolrPingResponse;
import org.apache.solr.client.solrj.response.UpdateResponse;
import org.apache.solr.client.solrj.request.UpdateRequest;
import org.apache.solr.common.SolrInputDocument;
import org.apache.solr.common.params.UpdateParams;

public class SolrEmitter implements Emitter {

    private final SolrClient solrClient;
    private final String baseUrl;

    public SolrEmitter() throws Exception {
	this.baseUrl = System.getenv("SOLR_URL");
	if (baseUrl == null) {
	    throw new RuntimeException("SOLR_URL is not set.");
	}

	this.solrClient = new HttpSolrClient.Builder(baseUrl).build();
    }

    public String getBaseUrl() {
	return baseUrl;
    }

    private SolrClient getSolrClient() {
	return solrClient;
    }

    @Override
    public String toString() {
	return "SolrEmitter (" + getBaseUrl() + ")";
    }

    @Override
    public void emit(Alteration alteration) throws Exception {
	// Ensure that Solr is responding before we continue ...
        int pings = 0;
	while (true) {
            try {
		SolrPingResponse pong = getSolrClient().ping();
		if (pong.getStatus() == 0) break;
            } catch (SolrServerException e) {
                System.out.println("Solr server error: " + e.getMessage());
            }

	    if (++pings < 5) {
		Thread.sleep(10000);
	    } else {
		throw new RuntimeException("Unable to successfully ping Solr.");
	    }
	}

	SolrInputDocument doc = new SolrInputDocument();
	doc.addField("id", UUID.randomUUID().toString());
	doc.addField("file_name", alteration.getFile().getName());
	doc.addField("file_path", alteration.getFile().getAbsolutePath());
	doc.addField("is_dir", alteration.getFile().isDirectory());
	doc.addField("file_size", alteration.getFile().length());
	doc.addField("mod_time", alteration.getModificationDate().format(DateTimeFormatter.ISO_INSTANT));
	doc.addField("file_exists", alteration.getFile().exists());
	doc.addField("event_type", alteration.getAltType());
	doc.addField("event_time", alteration.getTimestamp().format(DateTimeFormatter.ISO_INSTANT));

	UpdateResponse reponse = getSolrClient().add(doc, 15000);
    }

}
