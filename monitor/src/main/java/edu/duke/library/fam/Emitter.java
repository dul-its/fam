package edu.duke.library.fam;

import java.io.IOException;

public interface Emitter {

    public void emit(Alteration alteration) throws Exception;

}
