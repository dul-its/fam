/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.duke.library.fam;

import org.apache.commons.io.monitor.FileAlterationListenerAdaptor;
import org.apache.commons.io.monitor.FileAlterationObserver;

import java.io.File;
import java.time.OffsetDateTime;

/**
 *
 * @author dc
 */
public class Listener extends FileAlterationListenerAdaptor {

    public final static String CHANGE = "change";
    public final static String DELETE = "delete";
    public final static String CREATE = "create";
    public final static String FILE = "file";
    public final static String DIRECTORY = "directory";

    private Emitter emitter;

    public Listener(Emitter emitter) {
        this.setEmitter(emitter);
    }

    public void onDirectoryChange(File directory) {
        handleAlteration(directory, DIRECTORY, CHANGE);
    }

    public void onDirectoryCreate(File directory) {
        handleAlteration(directory, DIRECTORY, CREATE);
    }

    public void onDirectoryDelete(File directory) {
        handleAlteration(directory, DIRECTORY, DELETE);
    }
        
    public void onFileChange(File file) {
        handleAlteration(file, FILE, CHANGE);
    }

    public void onFileCreate(File file) {
        handleAlteration(file, FILE, CREATE);
    }

    public void onFileDelete(File file) {
        handleAlteration(file, FILE, DELETE);
    }

    public void onStart(FileAlterationObserver observer) {
        // System.out.println("Observer started.");
    }

    public void onStop(FileAlterationObserver observer) {
        // System.out.println("Observer stopped.");
    }

    public void handleAlteration(File file, String fileType, String altType) {
        Alteration alteration = new Alteration(file, fileType, altType, now());
        emit(alteration);
    }
    
    private void emit(Alteration alteration) {
        try {
            getEmitter().emit(alteration);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    private OffsetDateTime now() {
        return OffsetDateTime.now();
    }

    public Emitter getEmitter() {
        return emitter;
    }

    public void setEmitter(Emitter emitter) {
        this.emitter = emitter;
    }
}
