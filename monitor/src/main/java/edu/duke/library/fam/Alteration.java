/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.duke.library.fam;
import java.io.File;

import java.time.Instant;
import java.time.OffsetDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;

import javax.json.Json;
import javax.json.JsonObject;

/**
 *
 * @author dc
 */
public class Alteration {

    private final File file;
    private final String fileType;
    private final String altType;
    private final OffsetDateTime timestamp;

    public Alteration(File path, String fileType, String altType, OffsetDateTime timestamp) {
        this.file = path;
        this.fileType = fileType;
        this.altType = altType;
        this.timestamp = timestamp;
    }

    /**
     * @return the file
     */
    public File getFile() {
        return file;
    }

    /**
     * @return the alteration type
     */
    public String getAltType() {
        return altType;
    }

    /**
     * @return the timestamp
     */
    public OffsetDateTime getTimestamp() {
        return timestamp;
    }

    public String getFileType() {
        return fileType;
    }

    @Override
    public String toString() {
        return toJson().toString();
    }

    public JsonObject toJson() {
        return Json.createObjectBuilder()
	    .add("event_time", getTimestamp().format(DateTimeFormatter.ISO_INSTANT))
	    .add("event_type", getAltType())
	    // file_exists can be false negative
	    .add("file_exists", getFile().exists())
	    .add("file_name", getFile().getName())
	    .add("file_path", getFile().getAbsolutePath())
	    // file_size can be false 0
	    .add("file_size", getFile().length())
	    // is_dir can be false negative
	    .add("is_dir", getFile().isDirectory())
	    // mod_time can be false (0 epoch ms)
	    .add("mod_time", getModificationDate().format(DateTimeFormatter.ISO_INSTANT))
	    .build();
    }

    public OffsetDateTime getModificationDate() {
	Instant instant = Instant.ofEpochMilli(getFile().lastModified());
	return OffsetDateTime.ofInstant(instant, ZoneId.systemDefault());
    }

}
