# FAM: File Alteration Monitor

This project is built on the [Apache Commons IO](https://commons.apache.org/proper/commons-io/) library,
specifically leveraging the [File Monitor](https://commons.apache.org/proper/commons-io/javadocs/api-release/index.html?org/apache/commons/io/monitor/package-summary.html) component.

The tool is designed to monitor a single directory and emit messages
to either STDOUT (default) or to an AMQP server.

## Usage

    $ docker build -t fam -f ./Dockerfile ./fam
    $ docker run --rm -v /my/dir:/data fam

## Environment Variables

	FAM_PATH   Path to directory to monitor
	FAM_QUEUE  Name of queue to send messages to
	AMQP_URL   URL of message server
