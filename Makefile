SHELL = /bin/bash

build_tag ?= fam

.PHONY : build
build:
	docker build -t $(build_tag) ./monitor

.PHONY : clean
clean:
	rm -rf ./data/*

.PHONY : test
test:
	docker run --rm -e FAM_PATH=/data $(build_tag) java -jar fam.jar -h
