package main

import (
	"bytes"
	"encoding/json"
	"errors"
	"io"
	"io/fs"
	"log"
	"net/http"
	"net/url"
	"os"
	"path/filepath"
	"time"

	"github.com/google/uuid"
)

const CrawlEvent = "crawl"

type SolrDocument struct {
	Id          string  `json:"id"`
	EventTime   string  `json:"event_time"`
	EventType   string  `json:"event_type"`
	FileExists  bool    `json:"file_exists"`
	FileName    string  `json:"file_name"`
	FilePath    string  `json:"file_path"`
	FileSize    int64   `json:"file_size,omitempty"`
	IsDir       bool    `json:"is_dir"`
	ModTime     string  `json:"mod_time,omitempty"`
}

type SolrResponse struct {
	ResponseHeader struct {
		Status int
	}
	Status string // present in ping response
}

func pingSolr(pingUrl *url.URL) error {
	resp, err := http.Get(pingUrl.String())
	if err != nil {
		log.Printf("Solr ping request error: %s", err)
		return err
	}

	log.Printf("Solr ping response: %s", resp.Status)
	if resp.StatusCode != http.StatusOK {
		return errors.New("Solr ping response error")
	}

	// 200 OK
	body, err := io.ReadAll(resp.Body)
	resp.Body.Close()
	if err != nil {
		log.Printf("Error reading Solr ping response body: %s", err)
		return err
	}

	var pong SolrResponse
	err = json.Unmarshal(body, &pong)
	if err != nil {
		log.Printf("Error unmarshaling Solr ping response: %s", err)
		return err
	}

	log.Printf("Solr ping status: %s", pong.Status)
	if pong.Status == "OK" {
		return nil
	}

	return errors.New("Solr ping status not OK")
}

func waitForSolr(solrUrl *url.URL) error {
	pings := 60
	wait, _ := time.ParseDuration("1s")
	pingUrl, _ := url.Parse(solrUrl.String() + "/admin/ping")

	ping := 0
	for {
		err := pingSolr(pingUrl)

		ping++
		if err != nil && ping < pings {
			time.Sleep(wait)
		} else {
			return err
		}
	}
}

func solrTime(t time.Time) string {
	return t.UTC().Format(time.RFC3339)
}

func main() {
	solrRawUrl, found := os.LookupEnv("SOLR_URL")
	if !found {
		log.Fatalf("Required var not set: SOLR_URL")
	}
	solrUrl, err := url.Parse(solrRawUrl)
	if err != nil {
		log.Fatalf("Error parsing Solr URL: %s", err)
	}

	root, found := os.LookupEnv("ROOT")
	if !found {
		log.Fatalf("Required var not set: ROOT")
	}

	// wait for Solr to be fully available
	waitForSolr(solrUrl)

	// construct Solr update URL
	solrUpdateUrl, _ := url.Parse(solrUrl.String() + "/update")
	query := solrUpdateUrl.Query()
	query.Set("json.command", "false")
	query.Set("update.chain", "dedupe")
	query.Set("commitWithin", "15000")
	solrUpdateUrl.RawQuery = query.Encode()

	filepath.WalkDir(root, func(path string, d fs.DirEntry, err error) error {
		if err != nil {
			log.Printf("Walk error, skipping directory entry %s: %s", d.Name(), err)
			return filepath.SkipDir
		}

		// THIS MAY BE CAUSING THE PROCESS TO CRASH
		// info, e := d.Info()
		// if e != nil {
		// 	log.Printf("Error retrieving directory entry info, skipping %s: %s", d.Name(), e)
		// 	return filepath.SkipDir
		// }

		doc := SolrDocument{
			Id: uuid.New().String(),
			EventTime: solrTime(time.Now()),
			EventType: CrawlEvent,
			FileExists: true,
			FileName: d.Name(),
			FilePath: path,
			//FileSize: info.Size(),
			IsDir: d.IsDir(),
			//ModTime: solrTime(info.ModTime()),
		}

		data, err := json.Marshal(doc)
		if err != nil { // we probably mucked up the schema
			log.Printf("Error marshaling Solr document data, aborting directory walk: %s", err)
			return err
		}
		log.Printf("Solr document: %s", data)

		resp, err := http.Post(solrUpdateUrl.String(), "application/json", bytes.NewReader(data))
		if err != nil { // Solr is borked, abort crawl
			log.Printf("Solr error, aborting directory walk: %s", err)
			return err
		}

		log.Printf("Solr request: %s", resp.Request.URL)
		log.Printf("Solr response: %s", resp.Status)

		if resp.StatusCode != http.StatusOK {
			return errors.New("Solr HTTP response error, aborting directory walk")
		}

		return nil
	})
}
